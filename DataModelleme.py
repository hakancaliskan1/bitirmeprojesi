import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn import preprocessing
import matplotlib.pyplot as plt

dataset = pd.read_csv('/Users/mahmuterkanyesil/Downloads/top200.csv')

dataset.dropna(inplace=True)

print(dataset.head())

print(dataset.shape)

print(dataset.columns)

x = dataset.iloc[:-1].values
print(x)

le = LabelEncoder()

for i in range(19):
    x[:,i] = le.fit_transform(x[:,i])
   
x[:,0] = le.fit_transform(x[:,0])
ohe = OneHotEncoder(categorical_features = [0])
x = ohe.fit_transform(x).toarray()

print(x)

scaled_data = preprocessing.scale(x.T)

pca = PCA()
pca.fit(scaled_data)
pca.fit(scaled_data)
pca_data = pca.transform(scaled_data)

per_var = np.round(pca.explained_variance_ratio_*100, decimals=1)

labels = ['PC' + str(x) for x in range(1, len(per_var)+1)]

plt.bar(x = range(1, len(per_var)+1), height = per_var, tick_label = labels)
plt.ylabel('Percantage of Explained Variance')
plt.xlabel('Principal Component')
plt.title('Scree Plot')
plt.show()